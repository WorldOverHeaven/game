﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{

    void OnMouseUpAsButton()
    {
        switch (gameObject.name)
        {
            case "play":
                SceneManager.LoadScene("GameScene");
                break;
            case "score":
                SceneManager.LoadScene("Score");
                break;
            case "settings":
                SceneManager.LoadScene("settings");
                break;
            case "skin":
                SceneManager.LoadScene("Skins");
                break;
        }
    }
}
