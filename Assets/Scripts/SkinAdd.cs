﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinAdd : MonoBehaviour
{
    public GameObject script;

    void Awake()
    {

    }   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseUpAsButton()
    {
        switch (gameObject.name)
        {
            case "red":
            script.GetComponent<ChangeSkin>().addColor1();
                break;
            case "green":
            script.GetComponent<ChangeSkin>().addColor2();
                break;
            case "blue":
            script.GetComponent<ChangeSkin>().addColor3();
                break;
        }
    }
}
