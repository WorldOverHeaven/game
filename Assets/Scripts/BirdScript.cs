﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class BirdScript : MonoBehaviour
{
    public PathCreator pathCreator;
    public float move_Speed = 2.7f;
    float distanceTravelled;

    private bool canMove;
    private float min_x = -2.3f, max_x = 2.3f;
    private bool bird_direction;
    
    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        bird_direction = true;

    }

    // Update is called once per frame
    void Update()
    {
        MoveBird();
        //distanceTravelled += move_Speed * Time.deltaTime;
        //transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
    }

    void MoveBird()
    {
        if (canMove)
        {
            //Vector3 temp = transform.position;
            //temp.x += move_Speed * Time.deltaTime;;
            distanceTravelled += move_Speed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
        }

        if ((transform.position.x >= max_x) && (bird_direction == true))
            {
                //move_Speed *= -1f;
                transform.Rotate(0f, 180f, 0f);
                bird_direction = false;
            }
            else if ((transform.position.x <= min_x) && (bird_direction==false))
            {
                //move_Speed *= -1f;
                transform.Rotate(0f, 180f, 0f);
                bird_direction = true;
            }

            //transform.position = temp;
        }
    }

