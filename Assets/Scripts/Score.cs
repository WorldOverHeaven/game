﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public GameObject display;

    public int scoreGame = 0;

    public void AddScore()
    {
        scoreGame += 1;
        display.GetComponent<Text>().text = "SCORE " + scoreGame;  
    }

    // void OnGUI()
    // {
    //     GUIStyle styleTime = new GUIStyle();

    //     styleTime.fontSize = 70;

    //     GUI.Box(new Rect(90, 10, 400, 200), "Score " + scoreGame.ToString(), styleTime);
    // }
    // Start is called before the first frame update
    void Start()
    {
        display.GetComponent<Text>().text = "SCORE " + 0;   
    }

    // Update is called once per frame
    void Update()
    {

    }
}
