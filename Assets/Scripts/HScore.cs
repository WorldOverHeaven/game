﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HScore : MonoBehaviour
{
    public GameObject display;
    
    public int highScore;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("SaveScore")) {
             highScore = PlayerPrefs.GetInt("SaveScore");
        } 
        else {
            highScore = 0;
            PlayerPrefs.SetInt("SaveScore", highScore);
        }


        display.GetComponent<Text>().text = "YOUR BEST SCORE\n" + highScore;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
