﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSkin : MonoBehaviour
{
    public GameObject display1, display2, display3;
    
    public int color1, color2, color3;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("color1")) {
             color1 = PlayerPrefs.GetInt("color1");
        } 
        else {
            PlayerPrefs.SetInt("color2", 2);
            color1 = 2;
        }

        if (PlayerPrefs.HasKey("color2")) {
             color2 = PlayerPrefs.GetInt("color2");
        } 
        else {
            PlayerPrefs.SetInt("color2", 2);
            color2 = 2;
        }

        if (PlayerPrefs.HasKey("color3")) {
             color3 = PlayerPrefs.GetInt("color3");
        } 
        else {
            PlayerPrefs.SetInt("color3", 2);
            color3 = 2;
        }
    
        display1.GetComponent<Text>().text = "RED " + color1;
        display2.GetComponent<Text>().text = "RED " + color2;
        display3.GetComponent<Text>().text = "RED " + color3;
    }


    public void addColor1() {
        color1 += 1;
        color1 %= 10;
        PlayerPrefs.SetInt("color1", color1);
    }

    public void addColor2() {
        color2 += 1;
        color2 %= 10;
        PlayerPrefs.SetInt("color2", color2);
    }

    public void addColor3() {
        color3 += 1;
        color3 %= 10;
        PlayerPrefs.SetInt("color3", color3);
    }



    // Update is called once per frame
    void Update()
    {
        display1.GetComponent<Text>().text = "RED " + color1;
        display2.GetComponent<Text>().text = "GREEN " + color2;
        display3.GetComponent<Text>().text = "BLUE " + color3;
    }
}
