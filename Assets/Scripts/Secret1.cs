﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Secret1 : MonoBehaviour
{
    private int count = 3;

    float color1, color2, color3;

    private void Awake() {
        if (PlayerPrefs.HasKey("color1")) {
            color1 = PlayerPrefs.GetInt("color1");
        } 
        else {
            PlayerPrefs.SetInt("color2", 2);
            color1 = 2;
        }
        if (PlayerPrefs.HasKey("color2")) {
            color2 = PlayerPrefs.GetInt("color2");
        } 
        else {
            PlayerPrefs.SetInt("color2", 2);
            color2 = 2;
        }

        if (PlayerPrefs.HasKey("color3")) {
            color3 = PlayerPrefs.GetInt("color3");
        } 
        else {
            PlayerPrefs.SetInt("color3", 2);
            color3 = 2;
        }
        Color cl = new Vector4(color1 / 10, color2 / 10, color3 / 10, 1);
        gameObject.GetComponent<MeshRenderer>().material.color = cl;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void NewSp()
    {
        Color cl = new Vector4(1 - color1 / 10, 1 - color2 / 10, 1 - color3 / 10, 1);
        gameObject.GetComponent<MeshRenderer>().material.color = cl;
    }

    private void OnMouseDown()
    {
        count -= 1;
        if (count == 0)
        {
            count = 3;
            NewSp();
            //Ivoke("OldSp", 5f);
        }
    }
}
