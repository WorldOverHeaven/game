﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxUpdateColor : MonoBehaviour
{
    float color1, color2, color3;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("color1")) {
             color1 = PlayerPrefs.GetInt("color1");
        } 
        else {
            PlayerPrefs.SetInt("color2", 2);
            color1 = 2f;
        }

        if (PlayerPrefs.HasKey("color2")) {
             color2 = PlayerPrefs.GetInt("color2");
        } 
        else {
            PlayerPrefs.SetInt("color2", 2);
            color2 = 2f;
        }

        if (PlayerPrefs.HasKey("color3")) {
             color3 = PlayerPrefs.GetInt("color3");
        } 
        else {
            PlayerPrefs.SetInt("color3", 2);
            color3 = 2f;
        }
        
        Color cl = new Vector4(color1 / 10, color2 / 10, color1 / 10, 1);
        gameObject.GetComponent<MeshRenderer>().material.color = cl;
    }

    void ChangeColor() {
        color1 = PlayerPrefs.GetInt("color1");
        color2 = PlayerPrefs.GetInt("color2");
        color3 = PlayerPrefs.GetInt("color3");
        Color cl = new Vector4(color1 / 10, color2 / 10, color3 / 10, 1);
        gameObject.GetComponent<MeshRenderer>().material.color = cl;
    }
    // Update is called once per frame
    void Update()
    {
        ChangeColor();
    }
}
