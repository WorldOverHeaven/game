﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuUI;
    
    // Start is called before the first frame update
    void Awake()
    {
        //pauseMenuUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseUpAsButton()
    {
        switch (gameObject.name)
        {
            case "PAUSE":
                pauseMenuUI.SetActive(true);
                Time.timeScale = 0f;
                break;
            case "RESUME":
                pauseMenuUI.SetActive(false);
                Time.timeScale = 1f;
                break;
            case "MENU":
                Time.timeScale = 1f;
                SceneManager.LoadScene("Menu");
                break;
        }
    }
}
